﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class ChiHoi_HoatDongController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: ChiHoi_HoatDong/Create
        public ActionResult Create()
        {
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH");
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong");
            return View();
        }

        // POST: ChiHoi_HoatDong/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDChiHoi,IDHoatDong,GhiChu")] ChiHoi_HoatDong chiHoi_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.ChiHoi_HoatDong.Add(chiHoi_HoatDong);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", chiHoi_HoatDong.IDChiHoi);
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", chiHoi_HoatDong.IDHoatDong);
            return View(chiHoi_HoatDong);
        }

        // GET: ChiHoi_HoatDong/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiHoi_HoatDong chiHoi_HoatDong = db.ChiHoi_HoatDong.Find(id);
            if (chiHoi_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(chiHoi_HoatDong);
        }

        // POST: ChiHoi_HoatDong/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChiHoi_HoatDong chiHoi_HoatDong = db.ChiHoi_HoatDong.Find(id);
            db.ChiHoi_HoatDong.Remove(chiHoi_HoatDong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: ChiHoi_HoatDong/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiHoi_HoatDong chiHoi_HoatDong = db.ChiHoi_HoatDong.Find(id);
            if (chiHoi_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(chiHoi_HoatDong);
        }

        // GET: ChiHoi_HoatDong/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiHoi_HoatDong chiHoi_HoatDong = db.ChiHoi_HoatDong.Find(id);
            if (chiHoi_HoatDong == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", chiHoi_HoatDong.IDChiHoi);
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", chiHoi_HoatDong.IDHoatDong);
            return View(chiHoi_HoatDong);
        }

        // POST: ChiHoi_HoatDong/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDChiHoi,IDHoatDong,GhiChu")] ChiHoi_HoatDong chiHoi_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chiHoi_HoatDong).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", chiHoi_HoatDong.IDChiHoi);
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", chiHoi_HoatDong.IDHoatDong);
            return View(chiHoi_HoatDong);
        }

        // GET: ChiHoi_HoatDong
        public ActionResult Index()
        {
            var chiHoi_HoatDong = db.ChiHoi_HoatDong.Include(c => c.ChiHoi).Include(c => c.HoatDong);
            return View(chiHoi_HoatDong.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}