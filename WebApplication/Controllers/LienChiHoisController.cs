﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class LienChiHoisController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: LienChiHois/Create
        public ActionResult Create()
        {
            ViewBag.IDKhoa = new SelectList(db.Khoas, "IDKhoa", "MaKhoa");
            return View();
        }

        // POST: LienChiHois/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDLienChiHoi,MaLCH,TenLCH,IDKhoa,TrangThai")] LienChiHoi lienChiHoi)
        {
            if (ModelState.IsValid)
            {
                db.LienChiHois.Add(lienChiHoi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDKhoa = new SelectList(db.Khoas, "IDKhoa", "MaKhoa", lienChiHoi.IDKhoa);
            return View(lienChiHoi);
        }

        // GET: LienChiHois/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LienChiHoi lienChiHoi = db.LienChiHois.Find(id);
            if (lienChiHoi == null)
            {
                return HttpNotFound();
            }
            return View(lienChiHoi);
        }

        // POST: LienChiHois/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LienChiHoi lienChiHoi = db.LienChiHois.Find(id);
            db.LienChiHois.Remove(lienChiHoi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: LienChiHois/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LienChiHoi lienChiHoi = db.LienChiHois.Find(id);
            if (lienChiHoi == null)
            {
                return HttpNotFound();
            }
            return View(lienChiHoi);
        }

        // GET: LienChiHois/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LienChiHoi lienChiHoi = db.LienChiHois.Find(id);
            if (lienChiHoi == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDKhoa = new SelectList(db.Khoas, "IDKhoa", "MaKhoa", lienChiHoi.IDKhoa);
            return View(lienChiHoi);
        }

        // POST: LienChiHois/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDLienChiHoi,MaLCH,TenLCH,IDKhoa,TrangThai")] LienChiHoi lienChiHoi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lienChiHoi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDKhoa = new SelectList(db.Khoas, "IDKhoa", "MaKhoa", lienChiHoi.IDKhoa);
            return View(lienChiHoi);
        }

        // GET: LienChiHois
        public ActionResult Index()
        {
            var lienChiHois = db.LienChiHois.Include(l => l.Khoa);
            return View(lienChiHois.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}