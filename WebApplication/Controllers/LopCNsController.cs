﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class LopCNsController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: LopCNs/Create
        public ActionResult Create()
        {
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH");
            return View();
        }

        // POST: LopCNs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDLopCN,MaLopCN,TenLopCN,NienKhoa,IDChiHoi")] LopCN lopCN)
        {
            if (ModelState.IsValid)
            {
                db.LopCNs.Add(lopCN);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", lopCN.IDChiHoi);
            return View(lopCN);
        }

        // GET: LopCNs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LopCN lopCN = db.LopCNs.Find(id);
            if (lopCN == null)
            {
                return HttpNotFound();
            }
            return View(lopCN);
        }

        // POST: LopCNs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LopCN lopCN = db.LopCNs.Find(id);
            db.LopCNs.Remove(lopCN);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: LopCNs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LopCN lopCN = db.LopCNs.Find(id);
            if (lopCN == null)
            {
                return HttpNotFound();
            }
            return View(lopCN);
        }

        // GET: LopCNs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LopCN lopCN = db.LopCNs.Find(id);
            if (lopCN == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", lopCN.IDChiHoi);
            return View(lopCN);
        }

        // POST: LopCNs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDLopCN,MaLopCN,TenLopCN,NienKhoa,IDChiHoi")] LopCN lopCN)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lopCN).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", lopCN.IDChiHoi);
            return View(lopCN);
        }

        // GET: LopCNs
        public ActionResult Index()
        {
            var lopCNs = db.LopCNs.Include(l => l.ChiHoi);
            return View(lopCNs.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}