﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class Khoa_HoatDongController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: Khoa_HoatDong/Create
        public ActionResult Create()
        {
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong");
            ViewBag.IDKhoa = new SelectList(db.Khoas, "IDKhoa", "MaKhoa");
            return View();
        }

        // POST: Khoa_HoatDong/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDKhoa,IDHoatDong,GhiChu")] Khoa_HoatDong khoa_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.Khoa_HoatDong.Add(khoa_HoatDong);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", khoa_HoatDong.IDHoatDong);
            ViewBag.IDKhoa = new SelectList(db.Khoas, "IDKhoa", "MaKhoa", khoa_HoatDong.IDKhoa);
            return View(khoa_HoatDong);
        }

        // GET: Khoa_HoatDong/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Khoa_HoatDong khoa_HoatDong = db.Khoa_HoatDong.Find(id);
            if (khoa_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(khoa_HoatDong);
        }

        // POST: Khoa_HoatDong/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Khoa_HoatDong khoa_HoatDong = db.Khoa_HoatDong.Find(id);
            db.Khoa_HoatDong.Remove(khoa_HoatDong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Khoa_HoatDong/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Khoa_HoatDong khoa_HoatDong = db.Khoa_HoatDong.Find(id);
            if (khoa_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(khoa_HoatDong);
        }

        // GET: Khoa_HoatDong/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Khoa_HoatDong khoa_HoatDong = db.Khoa_HoatDong.Find(id);
            if (khoa_HoatDong == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", khoa_HoatDong.IDHoatDong);
            ViewBag.IDKhoa = new SelectList(db.Khoas, "IDKhoa", "MaKhoa", khoa_HoatDong.IDKhoa);
            return View(khoa_HoatDong);
        }

        // POST: Khoa_HoatDong/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDKhoa,IDHoatDong,GhiChu")] Khoa_HoatDong khoa_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(khoa_HoatDong).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", khoa_HoatDong.IDHoatDong);
            ViewBag.IDKhoa = new SelectList(db.Khoas, "IDKhoa", "MaKhoa", khoa_HoatDong.IDKhoa);
            return View(khoa_HoatDong);
        }

        // GET: Khoa_HoatDong
        public ActionResult Index()
        {
            var khoa_HoatDong = db.Khoa_HoatDong.Include(k => k.HoatDong).Include(k => k.Khoa);
            return View(khoa_HoatDong.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}