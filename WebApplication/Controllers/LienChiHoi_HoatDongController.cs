﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class LienChiHoi_HoatDongController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: LienChiHoi_HoatDong/Create
        public ActionResult Create()
        {
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong");
            ViewBag.IDLienChiHoi = new SelectList(db.LienChiHois, "IDLienChiHoi", "MaLCH");
            return View();
        }

        // POST: LienChiHoi_HoatDong/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDHoatDong,IDLienChiHoi,GhiChu")] LienChiHoi_HoatDong lienChiHoi_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.LienChiHoi_HoatDong.Add(lienChiHoi_HoatDong);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", lienChiHoi_HoatDong.IDHoatDong);
            ViewBag.IDLienChiHoi = new SelectList(db.LienChiHois, "IDLienChiHoi", "MaLCH", lienChiHoi_HoatDong.IDLienChiHoi);
            return View(lienChiHoi_HoatDong);
        }

        // GET: LienChiHoi_HoatDong/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LienChiHoi_HoatDong lienChiHoi_HoatDong = db.LienChiHoi_HoatDong.Find(id);
            if (lienChiHoi_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(lienChiHoi_HoatDong);
        }

        // POST: LienChiHoi_HoatDong/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LienChiHoi_HoatDong lienChiHoi_HoatDong = db.LienChiHoi_HoatDong.Find(id);
            db.LienChiHoi_HoatDong.Remove(lienChiHoi_HoatDong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: LienChiHoi_HoatDong/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LienChiHoi_HoatDong lienChiHoi_HoatDong = db.LienChiHoi_HoatDong.Find(id);
            if (lienChiHoi_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(lienChiHoi_HoatDong);
        }

        // GET: LienChiHoi_HoatDong/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LienChiHoi_HoatDong lienChiHoi_HoatDong = db.LienChiHoi_HoatDong.Find(id);
            if (lienChiHoi_HoatDong == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", lienChiHoi_HoatDong.IDHoatDong);
            ViewBag.IDLienChiHoi = new SelectList(db.LienChiHois, "IDLienChiHoi", "MaLCH", lienChiHoi_HoatDong.IDLienChiHoi);
            return View(lienChiHoi_HoatDong);
        }

        // POST: LienChiHoi_HoatDong/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDHoatDong,IDLienChiHoi,GhiChu")] LienChiHoi_HoatDong lienChiHoi_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lienChiHoi_HoatDong).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", lienChiHoi_HoatDong.IDHoatDong);
            ViewBag.IDLienChiHoi = new SelectList(db.LienChiHois, "IDLienChiHoi", "MaLCH", lienChiHoi_HoatDong.IDLienChiHoi);
            return View(lienChiHoi_HoatDong);
        }

        // GET: LienChiHoi_HoatDong
        public ActionResult Index()
        {
            var lienChiHoi_HoatDong = db.LienChiHoi_HoatDong.Include(l => l.HoatDong).Include(l => l.LienChiHoi);
            return View(lienChiHoi_HoatDong.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}