﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebApplication.Controllers
{
    public class HoiViensController : Controller
    {
        public enum ErrorMessage
        {
            None,
            Error1,
            Error2,
            Error3,
            Error4,
            Error5,
            Error6,
            Error7
        }

        private QLHVEntities db = new QLHVEntities();

        // GET: HoiViens
        //public ActionResult Index()
        //{
        //    var hoiViens = db.HoiViens.Include(h => h.GioiTinh).Include(h => h.LopCN).Include(h => h.LopDC);
        //    return View(hoiViens.ToList());
        //}
        public ActionResult Index(ErrorMessage? errorMessage)
        {
            if (errorMessage == null || errorMessage == ErrorMessage.None)
            {
                var hoiViens = db.HoiViens.Include(h => h.GioiTinh).Include(h => h.LopCN).Include(h => h.LopDC);
                return View(hoiViens.ToList());
            }
            if (errorMessage == ErrorMessage.Error1)
                ViewBag.Error = "Vui lòng chọn file";
            else if (errorMessage == ErrorMessage.Error2)
                ViewBag.Error = "Vui lòng chọn đúng file Excel";
            else if (errorMessage == ErrorMessage.Error3)
                ViewBag.Error = "Có biến xảy ra trong quá trình đọc file";
            else if (errorMessage == ErrorMessage.Error4)
                ViewBag.Error = "Cột lớp ĐC có mã không tồn tại";
            else if (errorMessage == ErrorMessage.Error5)
                ViewBag.Error = "Có biến xảy ra trong quá trình đưa dữ liệu vào db";
            else if (errorMessage == ErrorMessage.Error6)
                ViewBag.Error = "Cột lớp CN có mã không tồn tại";
            else if (errorMessage == ErrorMessage.Error7)
                ViewBag.Error = "Cột ngày sinh bị lỗi";
            return View();
        }
        // GET: HoiViens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien hoiVien = db.HoiViens.Find(id);
            if (hoiVien == null)
            {
                return HttpNotFound();
            }
            return View(hoiVien);
        }

        // GET: HoiViens/Create
        public ActionResult Create()
        {
            ViewBag.IDGioiTinh = new SelectList(db.GioiTinhs, "IDGioiTinh", "GioiTinh1");
            ViewBag.IDLopCN = new SelectList(db.LopCNs, "IDLopCN", "MaLopCN");
            ViewBag.IDLopDC = new SelectList(db.LopDCs, "IDLopDC", "MaLopDC");
            return View();
        }

        // POST: HoiViens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDHoiVien,MaHV,HoTen,IDGioiTinh,NgaySinh,NienKhoa,IDLopDC,IDLopCN,MSSV,CoLaHoiVien,DoanVien,DangVien,Email,SDT,TrangThai,ChucVu")] HoiVien hoiVien)
        {
            if (ModelState.IsValid)
            {
                db.HoiViens.Add(hoiVien);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDGioiTinh = new SelectList(db.GioiTinhs, "IDGioiTinh", "GioiTinh1", hoiVien.IDGioiTinh);
            ViewBag.IDLopCN = new SelectList(db.LopCNs, "IDLopCN", "MaLopCN", hoiVien.IDLopCN);
            ViewBag.IDLopDC = new SelectList(db.LopDCs, "IDLopDC", "MaLopDC", hoiVien.IDLopDC);
            return View(hoiVien);
        }

        // GET: HoiViens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien hoiVien = db.HoiViens.Find(id);
            if (hoiVien == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDGioiTinh = new SelectList(db.GioiTinhs, "IDGioiTinh", "GioiTinh1", hoiVien.IDGioiTinh);
            ViewBag.IDLopCN = new SelectList(db.LopCNs, "IDLopCN", "MaLopCN", hoiVien.IDLopCN);
            ViewBag.IDLopDC = new SelectList(db.LopDCs, "IDLopDC", "MaLopDC", hoiVien.IDLopDC);
            return View(hoiVien);
        }

        // POST: HoiViens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDHoiVien,MaHV,HoTen,IDGioiTinh,NgaySinh,NienKhoa,IDLopDC,IDLopCN,MSSV,CoLaHoiVien,DoanVien,DangVien,Email,SDT,TrangThai,ChucVu")] HoiVien hoiVien)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoiVien).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDGioiTinh = new SelectList(db.GioiTinhs, "IDGioiTinh", "GioiTinh1", hoiVien.IDGioiTinh);
            ViewBag.IDLopCN = new SelectList(db.LopCNs, "IDLopCN", "MaLopCN", hoiVien.IDLopCN);
            ViewBag.IDLopDC = new SelectList(db.LopDCs, "IDLopDC", "MaLopDC", hoiVien.IDLopDC);
            return View(hoiVien);
        }

        // GET: HoiViens/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien hoiVien = db.HoiViens.Find(id);
            if (hoiVien == null)
            {
                return HttpNotFound();
            }
            return View(hoiVien);
        }

        // POST: HoiViens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HoiVien hoiVien = db.HoiViens.Find(id);
            db.HoiViens.Remove(hoiVien);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelfile)
        {
            if (excelfile == null || excelfile.ContentLength == 0)
            {
                return RedirectToAction("Index", new { errorMessage = ErrorMessage.Error1 });
            }
            else
            {
                if (!excelfile.FileName.EndsWith("xls") && !excelfile.FileName.EndsWith("xlsx"))
                {
                    return RedirectToAction("Index", new { errorMessage = ErrorMessage.Error2 });
                }
                else
                {
                    string path = Server.MapPath("~/Content/upload/" + DateTime.UtcNow.ToFileTimeUtc().ToString() + excelfile.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);
                    excelfile.SaveAs(path);

                    //read data
                    ErrorMessage e = ReadData(path);
                    return RedirectToAction("Index", new { errorMessage = e });
                }
            }
        }

        public ErrorMessage ReadData(string path)
        {
            //try
            //{
            Excel.Application application = new Excel.Application();
            Excel.Workbook workbook = application.Workbooks.Open(path);
            Excel.Worksheet worksheet = workbook.ActiveSheet;
            Excel.Range range = worksheet.UsedRange;

            //doc tung dong
            List<HoiVien> listHoiVien = new List<HoiVien>();
            List<LopDC> listLopDC = new List<LopDC>();
            List<LopCN> listLopCN = new List<LopCN>();
            for (int row = 7; row <= range.Rows.Count; row++)
            {
                HoiVien hv = new HoiVien();

                //ho ten
                string ho = ((Excel.Range)range.Cells[row, 2]).Text.ToLower().Trim();
                if (ho != null && ho.Length > 0)
                {

                    string ten = ((Excel.Range)range.Cells[row, 3]).Text.ToLower().Trim();
                    ho = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(ho);
                    ten = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(ten);
                    hv.HoTen = ho + " " + ten;

                    //gioi tinh
                    string gioiTinh = ((Excel.Range)range.Cells[row, 4]).Text.ToLower().Trim();
                    hv.IDGioiTinh = (gioiTinh != null && gioiTinh.Length > 0) ? 1 : 0;

                    //ngay sinh
                    try
                    {
                        string ngaySinh = ((Excel.Range)range.Cells[row, 5]).Text.ToLower().Trim();
                        hv.NgaySinh = DateTime.Parse(ngaySinh);
                    }
                    catch (Exception)
                    {
                        workbook.Close();
                        return ErrorMessage.Error7;
                    }

                    //mssv
                    hv.MSSV = ((Excel.Range)range.Cells[row, 6]).Text.ToUpper().Trim();

                    //nien khoa
                    hv.NienKhoa = int.Parse(((Excel.Range)range.Cells[row, 7]).Text.Trim());

                    //lop
                    string tenLopDC = ((Excel.Range)range.Cells[row, 8]).Text.ToUpper().Trim();
                    string tenLopCN = ((Excel.Range)range.Cells[row, 10]).Text.ToUpper().Trim();
                    string maLopDC = tenLopDC + "-K" + hv.NienKhoa;
                    string maLopCN = tenLopCN + "-K" + hv.NienKhoa;

                    if (maLopDC != null && maLopDC.Length > 4)
                    {
                        var lopDC = db.LopDCs.FirstOrDefault(l => l.MaLopDC == maLopDC);
                        if (lopDC != null)
                        {
                            hv.IDLopDC = lopDC.IDLopDC;
                        }
                        else
                        {
                            workbook.Close();
                            return ErrorMessage.Error4;
                        }
                    }
                    else if (maLopCN != null && maLopCN.Length > 4)
                    {
                        var lopCN = db.LopCNs.FirstOrDefault(l => l.MaLopCN == maLopCN);
                        if (lopCN != null)
                        {
                            hv.IDLopCN = lopCN.IDLopCN;
                        }
                        else
                        {
                            workbook.Close();
                            return ErrorMessage.Error6;
                        }
                    }

                    if (hv.IDLopDC < 1) hv.IDLopDC = 1;
                    if (hv.IDLopCN < 1) hv.IDLopCN = 1;

                    //hoi vien
                    hv.CoLaHoiVien = true;

                    //doan vien
                    string doanVien = ((Excel.Range)range.Cells[row, 12]).Text.ToLower().Trim();
                    hv.DoanVien = (doanVien != null && doanVien.Length > 0) ? true : false;

                    //dang vien
                    string dangVien = ((Excel.Range)range.Cells[row, 13]).Text.ToLower().Trim();
                    hv.DangVien = (dangVien != null && dangVien.Length > 0) ? true : false;

                    //email
                    hv.Email = ((Excel.Range)range.Cells[row, 14]).Text.ToLower().Trim();

                    //sdt
                    hv.SDT = ((Excel.Range)range.Cells[row, 15]).Text.Trim();

                    //chuc vu
                    hv.ChucVu = ((Excel.Range)range.Cells[row, 16]).Text.Trim();

                    //trang thai
                    hv.TrangThai = true;

                    listHoiVien.Add(hv);
                }
                else break;
            }
            workbook.Close();

            //Luu vao db
            try
            {
                db.HoiViens.AddRange(listHoiVien);
            }
            catch (Exception)
            {
                return ErrorMessage.Error5;
            }
            db.SaveChanges();

            return ErrorMessage.None;
            //}
            //catch (Exception)
            //{
            //    return ErrorMessage.Error3; //bat loi ntn
            //}
        }
    }
}
