﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;
using Excel = Microsoft.Office.Interop.Excel;

namespace WebApplication.Controllers
{
    public enum ErrorMessage
    {
        None,
        Error1,
        Error2,
        Error3,
        Error4,
        Error5
    }
    public class ChiHoisController : Controller
    {
        private QLHVEntities db = new QLHVEntities();
        // GET: ChiHois
        //public ActionResult Index()
        //{

        //}

        public ActionResult Index(ErrorMessage? errorMessage)
        {
            if (errorMessage == null || errorMessage == ErrorMessage.None)
            {
                var chiHois = db.ChiHois.Include(c => c.LienChiHoi);
                return View(chiHois.ToList());
            }
            if (errorMessage == ErrorMessage.Error1)
                ViewBag.Error = "Vui lòng chọn file";
            else if (errorMessage == ErrorMessage.Error2)
                ViewBag.Error = "Vui lòng chọn đúng file Excel";
            else if (errorMessage == ErrorMessage.Error3)
                ViewBag.Error = "Có biến xảy ra trong quá trình đọc file";
            else if (errorMessage == ErrorMessage.Error4)
                ViewBag.Error = "Cột mã khoa có mã không tồn tại";
            else if (errorMessage == ErrorMessage.Error5)
                ViewBag.Error = "Có biến xảy ra trong quá trình đưa dữ liệu vào db";
            return View();
        }

        // GET: ChiHois/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiHoi chiHoi = db.ChiHois.Find(id);
            if (chiHoi == null)
            {
                return HttpNotFound();
            }
            return View(chiHoi);
        }

        // GET: ChiHois/Create
        public ActionResult Create()
        {
            ViewBag.IDLienChiHoi = new SelectList(db.LienChiHois, "IDLienChiHoi", "MaLCH");
            return View();
        }

        // POST: ChiHois/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDChiHoi,MaCH,TenCH,IDLienChiHoi,NienKhoa")] ChiHoi chiHoi)
        {
            if (ModelState.IsValid)
            {
                db.ChiHois.Add(chiHoi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDLienChiHoi = new SelectList(db.LienChiHois, "IDLienChiHoi", "MaLCH", chiHoi.IDLienChiHoi);
            return View(chiHoi);
        }

        // GET: ChiHois/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiHoi chiHoi = db.ChiHois.Find(id);
            if (chiHoi == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDLienChiHoi = new SelectList(db.LienChiHois, "IDLienChiHoi", "MaLCH", chiHoi.IDLienChiHoi);
            return View(chiHoi);
        }

        // POST: ChiHois/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDChiHoi,MaCH,TenCH,IDLienChiHoi,NienKhoa")] ChiHoi chiHoi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chiHoi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDLienChiHoi = new SelectList(db.LienChiHois, "IDLienChiHoi", "MaLCH", chiHoi.IDLienChiHoi);
            return View(chiHoi);
        }

        // GET: ChiHois/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChiHoi chiHoi = db.ChiHois.Find(id);
            if (chiHoi == null)
            {
                return HttpNotFound();
            }
            return View(chiHoi);
        }

        // POST: ChiHois/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChiHoi chiHoi = db.ChiHois.Find(id);
            db.ChiHois.Remove(chiHoi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelfile)
        {
            if (excelfile == null || excelfile.ContentLength == 0)
            {
                return RedirectToAction("Index", new { errorMessage = ErrorMessage.Error1 });
            }
            else
            {
                if (!excelfile.FileName.EndsWith("xls") && !excelfile.FileName.EndsWith("xlsx"))
                {
                    return RedirectToAction("Index", new { errorMessage = ErrorMessage.Error2 });
                }
                else
                {
                    string path = Server.MapPath("~/Content/upload/" + DateTime.UtcNow.ToFileTimeUtc().ToString() + excelfile.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);
                    excelfile.SaveAs(path);

                    //read data
                    ErrorMessage e = ReadData(path);
                    return RedirectToAction("Index", new { errorMessage = e });
                }
            }
        }

        public ErrorMessage ReadData(string path)
        {
            try
            {
                Excel.Application application = new Excel.Application();
                Excel.Workbook workbook = application.Workbooks.Open(path);
                Excel.Worksheet worksheet = workbook.ActiveSheet;
                Excel.Range range = worksheet.UsedRange;

                //doc nien khoa 
                int nienKhoa = int.Parse(((Excel.Range)range.Cells[1, 4]).Text.Substring(1, 2));

                //doc danh sach cac LCH
                List<LienChiHoi> listLienChiHoi = new List<LienChiHoi>();
                for (int row = 3; row <= range.Rows.Count; row++)
                {
                    string maLCH = ((Excel.Range)range.Cells[row, 2]).Text.ToUpper().Trim();
                    if (maLCH != null && maLCH.Length > 0)
                    {
                        var lienChiHoi = db.LienChiHois.FirstOrDefault(l => l.MaLCH == maLCH && l.TrangThai == true);
                        if (lienChiHoi != null)
                        {
                            listLienChiHoi.Add(lienChiHoi);
                        }
                        else
                        {
                            workbook.Close();
                            return ErrorMessage.Error4;
                        }
                    }
                    else break;
                }

                //doc danh sach Chi hoi chuyen nganh: cot 4, 6, 9
                //doc danh sach Chi hoi dai cuong: cot 8
                int r = 3;
                List<ChiHoi> listChiHoiCN = new List<ChiHoi>();
                List<ChiHoi> listChiHoiDC = new List<ChiHoi>();
                foreach (LienChiHoi lch in listLienChiHoi)
                {
                    string chs = ((Excel.Range)range.Cells[r, 4]).Text.ToUpper().Trim();
                    listChiHoiCN.AddRange(TachChiHoi(chs, nienKhoa, lch.IDLienChiHoi));

                    chs = ((Excel.Range)range.Cells[r, 6]).Text.ToUpper().Trim();
                    listChiHoiCN.AddRange(TachChiHoi(chs, nienKhoa + 1, lch.IDLienChiHoi));

                    chs = ((Excel.Range)range.Cells[r, 9]).Text.ToUpper().Trim();
                    listChiHoiCN.AddRange(TachChiHoi(chs, nienKhoa + 2, lch.IDLienChiHoi));

                    chs = ((Excel.Range)range.Cells[r, 8]).Text.ToUpper().Trim();
                    listChiHoiDC.AddRange(TachChiHoi(chs, nienKhoa + 2, lch.IDLienChiHoi));

                    r++;
                }
                workbook.Close();

                //tao danh sach lop chuyen nganh
                List<LopCN> listLopCN = new List<LopCN>();
                foreach (ChiHoi ch in listChiHoiCN)
                {
                    ch.LopCNs = TachLopCN(ch);
                    listLopCN.AddRange(ch.LopCNs);
                }

                List<LopDC> listLopDC = new List<LopDC>();
                foreach (ChiHoi ch in listChiHoiDC)
                {
                    ch.LopDCs = TachLopDC(ch);
                    listLopDC.AddRange(ch.LopDCs);
                }

                //Luu vao db
                try
                {
                    db.ChiHois.AddRange(listChiHoiCN);
                    db.ChiHois.AddRange(listChiHoiDC);
                    db.LopCNs.AddRange(listLopCN);
                    db.LopDCs.AddRange(listLopDC);
                }
                catch (Exception)
                {
                    return ErrorMessage.Error5;
                }
                db.SaveChanges();

                return ErrorMessage.None;
            }
            catch (Exception)
            {
                return ErrorMessage.Error2;
            }
        }

        //Tach lop CN tu chi hoi: TH1,2,3
        public List<LopCN> TachLopCN(ChiHoi ch)
        {
            List<LopCN> listLopCN = new List<LopCN>();
            if (!ch.TenCH.Contains(","))
            {
                LopCN lcn = new LopCN();
                lcn.TenLopCN = ch.TenCH;
                lcn.NienKhoa = ch.NienKhoa;
                lcn.MaLopCN = ch.MaCH;
                lcn.IDChiHoi = ch.IDChiHoi;
                listLopCN.Add(lcn);
            }
            else
            {
                List<string> list = ch.TenCH.Split(',').ToList();
                string chu = "";
                foreach (char c in list.ElementAt(0))
                {
                    if (char.IsLetter(c))
                        chu += c.ToString();
                    else break;
                }

                foreach (string l in list)
                {
                    LopCN lcn = new LopCN();
                    if (char.IsDigit(l.ToCharArray()[0]))
                        lcn.TenLopCN = chu + l;
                    else lcn.TenLopCN = l;
                    lcn.NienKhoa = ch.NienKhoa;
                    lcn.MaLopCN = lcn.TenLopCN + "-K" + ch.NienKhoa;
                    lcn.IDChiHoi = ch.IDChiHoi;
                    listLopCN.Add(lcn);
                }
            }
            return listLopCN;
        }

        //Tach lop DC tu chi hoi DC: 01-02-03
        public List<LopDC> TachLopDC(ChiHoi ch)
        {
            List<LopDC> listLopDC = new List<LopDC>();
            if (!ch.TenCH.Contains("-"))
            {
                LopDC ldc = new LopDC();
                ldc.TenLopDC = "DC" + ch.TenCH;
                ldc.NienKhoa = ch.NienKhoa;
                ldc.MaLopDC = ch.MaCH;
                ldc.IDChiHoi = ch.IDChiHoi;
                listLopDC.Add(ldc);
            }
            else
            {
                List<string> list = ch.TenCH.Split('-').ToList();

                foreach (string l in list)
                {
                    LopDC ldc = new LopDC();
                    ldc.TenLopDC = "DC" + l;
                    ldc.NienKhoa = ch.NienKhoa;
                    ldc.MaLopDC = ldc.TenLopDC + "-K" + ch.NienKhoa;
                    ldc.IDChiHoi = ch.IDChiHoi;
                    listLopDC.Add(ldc);
                }
            }
            return listLopDC;
        }

        //Tach Chi hoi tu chuoiChiHoi co dang AD01;QT02; ...
        public List<ChiHoi> TachChiHoi(string chuoiChiHoi, int nienKhoa, int IDLienChiHoi)
        {
            List<ChiHoi> listChiHoi = new List<ChiHoi>();
            List<string> list = chuoiChiHoi.Split(';').ToList();
            foreach (string l in list)
                if (l != null && l.Trim().Length > 0)
                {
                    ChiHoi ch = new ChiHoi();
                    ch.TenCH = l.Trim().ToUpper();
                    ch.NienKhoa = nienKhoa;
                    ch.MaCH = ch.TenCH + "-K" + nienKhoa;
                    ch.IDLienChiHoi = IDLienChiHoi;
                    listChiHoi.Add(ch);
                }
            return listChiHoi;
        }
    }
}
