﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class CLBDN_HoatDongController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: CLBDN_HoatDong/Create
        public ActionResult Create()
        {
            ViewBag.IDCLBDN = new SelectList(db.CLBDNs, "IDCLBDN", "MaCLBDN");
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong");
            return View();
        }

        // POST: CLBDN_HoatDong/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDHoatDong,IDCLBDN,GhiChu")] CLBDN_HoatDong cLBDN_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.CLBDN_HoatDong.Add(cLBDN_HoatDong);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDCLBDN = new SelectList(db.CLBDNs, "IDCLBDN", "MaCLBDN", cLBDN_HoatDong.IDCLBDN);
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", cLBDN_HoatDong.IDHoatDong);
            return View(cLBDN_HoatDong);
        }

        // GET: CLBDN_HoatDong/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CLBDN_HoatDong cLBDN_HoatDong = db.CLBDN_HoatDong.Find(id);
            if (cLBDN_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(cLBDN_HoatDong);
        }

        // POST: CLBDN_HoatDong/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CLBDN_HoatDong cLBDN_HoatDong = db.CLBDN_HoatDong.Find(id);
            db.CLBDN_HoatDong.Remove(cLBDN_HoatDong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: CLBDN_HoatDong/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CLBDN_HoatDong cLBDN_HoatDong = db.CLBDN_HoatDong.Find(id);
            if (cLBDN_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(cLBDN_HoatDong);
        }

        // GET: CLBDN_HoatDong/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CLBDN_HoatDong cLBDN_HoatDong = db.CLBDN_HoatDong.Find(id);
            if (cLBDN_HoatDong == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDCLBDN = new SelectList(db.CLBDNs, "IDCLBDN", "MaCLBDN", cLBDN_HoatDong.IDCLBDN);
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", cLBDN_HoatDong.IDHoatDong);
            return View(cLBDN_HoatDong);
        }

        // POST: CLBDN_HoatDong/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDHoatDong,IDCLBDN,GhiChu")] CLBDN_HoatDong cLBDN_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cLBDN_HoatDong).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDCLBDN = new SelectList(db.CLBDNs, "IDCLBDN", "MaCLBDN", cLBDN_HoatDong.IDCLBDN);
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", cLBDN_HoatDong.IDHoatDong);
            return View(cLBDN_HoatDong);
        }

        // GET: CLBDN_HoatDong
        public ActionResult Index()
        {
            var cLBDN_HoatDong = db.CLBDN_HoatDong.Include(c => c.CLBDN).Include(c => c.HoatDong);
            return View(cLBDN_HoatDong.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}