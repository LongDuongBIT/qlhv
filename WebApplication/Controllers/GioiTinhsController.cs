﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class GioiTinhsController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: GioiTinhs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GioiTinhs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDGioiTinh,GioiTinh1")] GioiTinh gioiTinh)
        {
            if (ModelState.IsValid)
            {
                db.GioiTinhs.Add(gioiTinh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gioiTinh);
        }

        // GET: GioiTinhs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GioiTinh gioiTinh = db.GioiTinhs.Find(id);
            if (gioiTinh == null)
            {
                return HttpNotFound();
            }
            return View(gioiTinh);
        }

        // POST: GioiTinhs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GioiTinh gioiTinh = db.GioiTinhs.Find(id);
            db.GioiTinhs.Remove(gioiTinh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: GioiTinhs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GioiTinh gioiTinh = db.GioiTinhs.Find(id);
            if (gioiTinh == null)
            {
                return HttpNotFound();
            }
            return View(gioiTinh);
        }

        // GET: GioiTinhs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GioiTinh gioiTinh = db.GioiTinhs.Find(id);
            if (gioiTinh == null)
            {
                return HttpNotFound();
            }
            return View(gioiTinh);
        }

        // POST: GioiTinhs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDGioiTinh,GioiTinh1")] GioiTinh gioiTinh)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gioiTinh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gioiTinh);
        }

        // GET: GioiTinhs
        public ActionResult Index()
        {
            return View(db.GioiTinhs.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}