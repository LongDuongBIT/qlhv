﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HoiVien_HoatDongController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: HoiVien_HoatDong/Create
        public ActionResult Create()
        {
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong");
            ViewBag.IDHoiVien = new SelectList(db.HoiViens, "IDHoiVien", "MaHV");
            return View();
        }

        // POST: HoiVien_HoatDong/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDHoiVien,IDHoatDong,DanhGia,GhiChu,ChucVu")] HoiVien_HoatDong hoiVien_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.HoiVien_HoatDong.Add(hoiVien_HoatDong);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", hoiVien_HoatDong.IDHoatDong);
            ViewBag.IDHoiVien = new SelectList(db.HoiViens, "IDHoiVien", "MaHV", hoiVien_HoatDong.IDHoiVien);
            return View(hoiVien_HoatDong);
        }

        // GET: HoiVien_HoatDong/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien_HoatDong hoiVien_HoatDong = db.HoiVien_HoatDong.Find(id);
            if (hoiVien_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(hoiVien_HoatDong);
        }

        // POST: HoiVien_HoatDong/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HoiVien_HoatDong hoiVien_HoatDong = db.HoiVien_HoatDong.Find(id);
            db.HoiVien_HoatDong.Remove(hoiVien_HoatDong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: HoiVien_HoatDong/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien_HoatDong hoiVien_HoatDong = db.HoiVien_HoatDong.Find(id);
            if (hoiVien_HoatDong == null)
            {
                return HttpNotFound();
            }
            return View(hoiVien_HoatDong);
        }

        // GET: HoiVien_HoatDong/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien_HoatDong hoiVien_HoatDong = db.HoiVien_HoatDong.Find(id);
            if (hoiVien_HoatDong == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", hoiVien_HoatDong.IDHoatDong);
            ViewBag.IDHoiVien = new SelectList(db.HoiViens, "IDHoiVien", "MaHV", hoiVien_HoatDong.IDHoiVien);
            return View(hoiVien_HoatDong);
        }

        // POST: HoiVien_HoatDong/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDHoiVien,IDHoatDong,DanhGia,GhiChu,ChucVu")] HoiVien_HoatDong hoiVien_HoatDong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoiVien_HoatDong).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDHoatDong = new SelectList(db.HoatDongs, "IDHoatDong", "TenHoatDong", hoiVien_HoatDong.IDHoatDong);
            ViewBag.IDHoiVien = new SelectList(db.HoiViens, "IDHoiVien", "MaHV", hoiVien_HoatDong.IDHoiVien);
            return View(hoiVien_HoatDong);
        }

        // GET: HoiVien_HoatDong
        public ActionResult Index()
        {
            var hoiVien_HoatDong = db.HoiVien_HoatDong.Include(h => h.HoatDong).Include(h => h.HoiVien);
            return View(hoiVien_HoatDong.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}