﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class LopDCsController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: LopDCs/Create
        public ActionResult Create()
        {
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH");
            return View();
        }

        // POST: LopDCs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDLopDC,MaLopDC,TenLopDC,NienKhoa,IDChiHoi")] LopDC lopDC)
        {
            if (ModelState.IsValid)
            {
                db.LopDCs.Add(lopDC);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", lopDC.IDChiHoi);
            return View(lopDC);
        }

        // GET: LopDCs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LopDC lopDC = db.LopDCs.Find(id);
            if (lopDC == null)
            {
                return HttpNotFound();
            }
            return View(lopDC);
        }

        // POST: LopDCs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LopDC lopDC = db.LopDCs.Find(id);
            db.LopDCs.Remove(lopDC);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: LopDCs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LopDC lopDC = db.LopDCs.Find(id);
            if (lopDC == null)
            {
                return HttpNotFound();
            }
            return View(lopDC);
        }

        // GET: LopDCs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LopDC lopDC = db.LopDCs.Find(id);
            if (lopDC == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", lopDC.IDChiHoi);
            return View(lopDC);
        }

        // POST: LopDCs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDLopDC,MaLopDC,TenLopDC,NienKhoa,IDChiHoi")] LopDC lopDC)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lopDC).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDChiHoi = new SelectList(db.ChiHois, "IDChiHoi", "MaCH", lopDC.IDChiHoi);
            return View(lopDC);
        }

        // GET: LopDCs
        public ActionResult Index()
        {
            var lopDCs = db.LopDCs.Include(l => l.ChiHoi);
            return View(lopDCs.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}