﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HoiVien_CLBDNController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: HoiVien_CLBDN/Create
        public ActionResult Create()
        {
            ViewBag.IDCLBDN = new SelectList(db.CLBDNs, "IDCLBDN", "MaCLBDN");
            ViewBag.IDHoiVien = new SelectList(db.HoiViens, "IDHoiVien", "MaHV");
            return View();
        }

        // POST: HoiVien_CLBDN/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDHoiVien_CLBDN,IDHoiVien,IDCLBDN,NgayThamGia,ChucVu,DanhGiaHoatDongTichCuc,SoQuyetDinhChuanY,TomTatThanhTich,TrangThai")] HoiVien_CLBDN hoiVien_CLBDN)
        {
            if (ModelState.IsValid)
            {
                db.HoiVien_CLBDN.Add(hoiVien_CLBDN);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDCLBDN = new SelectList(db.CLBDNs, "IDCLBDN", "MaCLBDN", hoiVien_CLBDN.IDCLBDN);
            ViewBag.IDHoiVien = new SelectList(db.HoiViens, "IDHoiVien", "MaHV", hoiVien_CLBDN.IDHoiVien);
            return View(hoiVien_CLBDN);
        }

        // GET: HoiVien_CLBDN/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien_CLBDN hoiVien_CLBDN = db.HoiVien_CLBDN.Find(id);
            if (hoiVien_CLBDN == null)
            {
                return HttpNotFound();
            }
            return View(hoiVien_CLBDN);
        }

        // POST: HoiVien_CLBDN/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HoiVien_CLBDN hoiVien_CLBDN = db.HoiVien_CLBDN.Find(id);
            db.HoiVien_CLBDN.Remove(hoiVien_CLBDN);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: HoiVien_CLBDN/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien_CLBDN hoiVien_CLBDN = db.HoiVien_CLBDN.Find(id);
            if (hoiVien_CLBDN == null)
            {
                return HttpNotFound();
            }
            return View(hoiVien_CLBDN);
        }

        // GET: HoiVien_CLBDN/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HoiVien_CLBDN hoiVien_CLBDN = db.HoiVien_CLBDN.Find(id);
            if (hoiVien_CLBDN == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDCLBDN = new SelectList(db.CLBDNs, "IDCLBDN", "MaCLBDN", hoiVien_CLBDN.IDCLBDN);
            ViewBag.IDHoiVien = new SelectList(db.HoiViens, "IDHoiVien", "MaHV", hoiVien_CLBDN.IDHoiVien);
            return View(hoiVien_CLBDN);
        }

        // POST: HoiVien_CLBDN/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDHoiVien_CLBDN,IDHoiVien,IDCLBDN,NgayThamGia,ChucVu,DanhGiaHoatDongTichCuc,SoQuyetDinhChuanY,TomTatThanhTich,TrangThai")] HoiVien_CLBDN hoiVien_CLBDN)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hoiVien_CLBDN).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDCLBDN = new SelectList(db.CLBDNs, "IDCLBDN", "MaCLBDN", hoiVien_CLBDN.IDCLBDN);
            ViewBag.IDHoiVien = new SelectList(db.HoiViens, "IDHoiVien", "MaHV", hoiVien_CLBDN.IDHoiVien);
            return View(hoiVien_CLBDN);
        }

        // GET: HoiVien_CLBDN
        public ActionResult Index()
        {
            var hoiVien_CLBDN = db.HoiVien_CLBDN.Include(h => h.CLBDN).Include(h => h.HoiVien);
            return View(hoiVien_CLBDN.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}