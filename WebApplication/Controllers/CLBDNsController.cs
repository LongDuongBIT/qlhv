﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class CLBDNsController : Controller
    {
        private QLHVEntities db = new QLHVEntities();

        // GET: CLBDNs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CLBDNs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDCLBDN,MaCLBDN,TenCLBDN,ThuocCapTruong,LoaiCLBDN,MoTa,NgayThanhLap,SoQuyetDinhChuanY,TrangThai")] CLBDN cLBDN)
        {
            if (ModelState.IsValid)
            {
                db.CLBDNs.Add(cLBDN);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cLBDN);
        }

        // GET: CLBDNs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CLBDN cLBDN = db.CLBDNs.Find(id);
            if (cLBDN == null)
            {
                return HttpNotFound();
            }
            return View(cLBDN);
        }

        // POST: CLBDNs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CLBDN cLBDN = db.CLBDNs.Find(id);
            db.CLBDNs.Remove(cLBDN);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: CLBDNs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CLBDN cLBDN = db.CLBDNs.Find(id);
            if (cLBDN == null)
            {
                return HttpNotFound();
            }
            return View(cLBDN);
        }

        // GET: CLBDNs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CLBDN cLBDN = db.CLBDNs.Find(id);
            if (cLBDN == null)
            {
                return HttpNotFound();
            }
            return View(cLBDN);
        }

        // POST: CLBDNs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDCLBDN,MaCLBDN,TenCLBDN,ThuocCapTruong,LoaiCLBDN,MoTa,NgayThanhLap,SoQuyetDinhChuanY,TrangThai")] CLBDN cLBDN)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cLBDN).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cLBDN);
        }

        // GET: CLBDNs
        public ActionResult Index()
        {
            return View(db.CLBDNs.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}